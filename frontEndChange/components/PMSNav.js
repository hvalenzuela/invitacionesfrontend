import React, {Component} from 'react'
import {StyleSheet} from 'react-native';

import {
  ListItem,
  Item,
  Input,
  Body,
  Text,
  Left,
  Right,
  Label,
  View,
  Content,
  Button,
  Form,
  List,
  SwipeRow
} from 'native-base'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class PMSNav extends Component {
  render() {
    return (
      <Content>
        <Form>
          <List>
            <Item style={styles.addPms}>
              <Button transparent style={styles.addPmsbtn}>
                <Text style={styles.addPmstxt}>
                  AGREGAR PMS
                </Text>
                <Icon style={styles.secondaryIcon} name="person-add"/>
              </Button>
            </Item>
          </List>
        </Form>
      </Content>
    )
  }
}
