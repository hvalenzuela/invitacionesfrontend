import React, {Component} from 'react'
import {StyleSheet, Image, TextInput} from 'react-native';
import {
  Header,
  Container,
  Content,
  Footer,
  Form,
  Item,
  Input,
  Label,
  List,
  Body,
  Title,
  Button,
  Text
} from 'native-base'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SVGImage from 'react-native-svg-image'
import { TabNavigator } from 'react-navigation'
import TabsBar from './tabs'
import IndexMenu from './indexMenu'


export default class SignIn extends Component {
  static navigationOptions = {
    title: 'INICIO DE SESION',
    headerStyle: {
      borderBottomWidth: 2,
      borderBottomColor: '#a0915a'
    },
    headerTitleStyle: {
      color: '#a0915a'
    },
    headerBackTitleStyle: {
      color: '#a0915a'
    }

  }
  render() {
    return (
      <Container style={styles.contentBackColor}>
        <Content>
          <Item style={[
            styles.inlineCenterItem, {
              borderColor: 'transparent'
            }
          ]}>
            <Body>
              <SVGImage style={[{
                  width: 320,
                  height: 200
                }
              ]} source={{
                uri: 'https://bpprivilegeclub.com/imgs/logo_bppc.svg'
              }}/>
            </Body>
          </Item>
          <Form>
            <List style={styles.columnItem}>
              <Item style={[
                {
                  maxWidth: 300
                },
                styles.centerItem
              ]}>
                <Input placeholder='CODIGO EMPLEADO'/>
                <Icon active name='account-circle' style={styles.creamIcon}/>
              </Item>
              <Item style={[
                {
                  maxWidth: 300
                },
                styles.centerItem
              ]}>
                <Input secureTextEntry={true} placeholder='CONTRASEÑA'/>
                <Icon name='lock' style={styles.creamIcon}/>

              </Item>

            </List>
          </Form>
          <Item style={[
            styles.inlineCenterItem, {
              borderBottomWidth: 0
            }
          ]}>
            <Button iconRight full light style={[
              styles.loginBtn,
              styles.inlineItem, {
                minWidth: 300
              }
            ]} onPress={() => this.props.navigation.navigate('TabsBar')}>
              <Text style={styles.primarybtnText}>
                INICIAR
              </Text>
              <Icon style={[
                styles.creamIcon, {
                  marginRight: 0
                }
              ]} name="play-arrow"/>
            </Button>
          </Item>
        </Content>
        <Footer>
          <Text style={[styles.primarycolor, styles.inlineItem]}>
            © 2017 BAHIA PRINCIPE PRIVILEGE CLUB</Text>
        </Footer>
      </Container>
    )
  }
}
