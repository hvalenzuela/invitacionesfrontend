import React, {Component} from 'react'
import {StyleSheet, Alert} from 'react-native';

import {
  ListItem,
  Item,
  Input,
  Body,
  Text,
  Left,
  Right,
  Label,
  View,
  Content,
  Button,
  SwipeRow,
} from 'native-base'
import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import GiftsAdd from './giftsAdd'

var alertMessage = '¿Seguro que desea Eliminar el PMS?'


export default class PMSform extends Component {

  render() {
    return (
      <View>
        <SwipeRow
           rightOpenValue={-75}
           body={
             <Item style={{borderColor:'transparent'}}>
             <Icon name="perm-contact-calendar" style={[styles.creamIcon, {marginLeft:15}]}/>
             <Text style={styles.graycolor}>
               GUILLERMO LOPEZ
            </Text>
            <Text style={{
              marginLeft: 30
            }}>HABITACION
             81101
            </Text>
            </Item>
           }
           right={
             <Button danger onPress={() => Alert.alert(
            'CONFIRMAR',
            alertMessage,
            [
              {text: 'Cancelar', onPress: () => console.log('Cancel Pressed!')},
              {text: 'Aceptar', onPress: () => console.log('OK Pressed!')},
            ]
          )}>
              <Icon active name="delete-forever" style={styles.whiteIcon}/>
           </Button>
          }
       />
      </View>
    )
  }
}
