import React, {Component} from 'react'
import {StyleSheet, ListView, Alert} from 'react-native'
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Text,
  Separator,
  Item,
  Header,
  Title,
  List,
  ListItem,
  Input,
  View,
  Grid,
  Col,
  Row
} from 'native-base'

import styles from './styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SearchHeader from './search'
const datas = ["Simon Mignolet"]

var alertMessage = '¿Seguro que desea imprimir esta Invitación?'

class CompListView extends Component {

  static navigationOptions = {
    title: 'LISTADO',
    headerStyle: {
      borderBottomWidth: 2,
      borderBottomColor: '#a0915a'
    },
    headerTitleStyle: {
      color: '#a0915a'
    },
    headerBackTitleStyle: {
      color: '#a0915a'
    },
    headerLeft: (
      <Text></Text>
    ),
    tabBarLabel: 'LISTADO',
    tabBarIcon: ({tintColor}) => (<Icon name="view-list" style={[
      styles.iconSize, {
        color: tintColor
      }
    ]}/>)
  }
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      basic: true,
      listViewData: datas
    };
  }

  deleteRow(secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].props.closeRow();
    const newData = [...this.state.listViewData];
    newData.splice(rowId, 1);
    this.setState({listViewData: newData});
  }

  render() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    return (
      <Container style={styles.contentBackColor}>
        <SearchHeader/>

        <Item style={styles.inlineBtn}>
          <Button iconRight transparent full light style={[styles.inlineCenterItem, styles.activeState]}>
            <Text style={styles.stateText}>
              ALL
            </Text>

          </Button>
          <Button iconRight transparent full light style={[styles.inlineCenterItem]}>
            <Icon name="fiber-manual-record" style={{
              color: '#e0876a'
            }}/>
            <Text style={[
              styles.stateText, {
                marginLeft: -15
              }
            ]}>
              SCHEDULE
            </Text>

          </Button>
          <Button iconRight transparent full light style={[styles.centerItem]}>
            <Icon name="fiber-manual-record" style={{
              color: '#588c7e'
            }}/>
            <Text style={[
              styles.stateText, {
                marginLeft: -15
              }
            ]}>
              SHOW
            </Text>
          </Button>
          <Button iconRight transparent full light style={[styles.centerItem]}>
            <Icon name="fiber-manual-record" style={{
              color: '#ffcc5c'
            }}/>
            <Text style={[
              styles.stateText, {
                marginLeft: -15
              }
            ]}>
              NOSHOW
            </Text>
          </Button>
          <Button iconRight transparent full light style={[styles.centerItem]}>
            <Icon name="fiber-manual-record" style={{
              color: '#b0aac0'
            }}/>
            <Text style={[
              styles.stateText, {
                marginLeft: -15
              }
            ]}>
              INOUT
            </Text>

          </Button>
          <Button iconRight transparent full light style={[styles.centerItem]}>
            <Icon name="fiber-manual-record" style={{
              color: '#87bdd8'
            }}/>
            <Text style={[
              styles.stateText, {
                marginLeft: -15
              }
            ]}>
              RESCHEDULE
            </Text>
          </Button>
          <Button iconRight transparent full light style={[styles.centerItem]}>
            <Icon name="fiber-manual-record" style={{
              color: '#c94c4c'
            }}/>
            <Text style={[
              styles.stateText, {
                marginLeft: -15
              }
            ]}>
              CANCELED
            </Text>
          </Button>
        </Item>

        <Content>
          <Item style={[
            styles.inlineCenterItem,
            styles.addPms, {
              paddingTop: 10,
              paddingBottom: 10
            }
          ]}>
            <Grid>
              <Col size={1}></Col>
              <Col size={4} style={styles.centerItem}>
                <Text style={styles.addPmstxt}>
                  PROSPECTO
                </Text>
              </Col>
              <Col size={3} style={styles.centerItem}>
                <Text style={styles.addPmstxt}>
                  HABITACION
                </Text>
              </Col>
              <Col size={3} style={styles.centerItem}>
                <Text style={styles.addPmstxt}>
                  TIPO
                </Text>
              </Col>
              <Col size={3} style={styles.centerItem}>
                <Text style={styles.addPmstxt}>
                  OPC
                </Text>
              </Col>
              <Col size={3}></Col>
            </Grid>
          </Item>
          <List dataSource={this.ds.cloneWithRows(this.state.listViewData)} renderRow={data => <Item style={styles.inlineCenterItem}>
            <Grid>
              <Col size={1} style={styles.centerItem}>
                <Icon name="fiber-manual-record" style={{
                  color: '#e0876a',
                  fontSize: 18
                }}/>
              </Col>
              <Col size={4} style={styles.centerItem}>
                <Text>
                  {data}
                </Text>
              </Col>
              <Col size={3} style={styles.centerItem}>
                <Text style={styles.listText}>
                  10001
                </Text>
              </Col>
              <Col size={3} style={styles.centerItem}>
                <Text style={styles.listText}>
                  PM
                </Text>
              </Col>
              <Col size={3} style={styles.centerItem}>
                <Text style={styles.listText}>
                  10001
                </Text>
              </Col>
              <Col size={3} style={{
                alignItems: 'flex-end'
              }}>
                <Row>
                  <Button transparent onPress={() => this.props.navigation.navigate('PreviewForm')}>
                    <Icon name="description" style={styles.listIcon}/>
                  </Button>
                  <Button transparent onPress={() => this.props.navigation.navigate('CreateView')}>
                    <Icon name="mode-edit" style={styles.listIcon}/>
                  </Button>
                  <Button transparent onPress={() => Alert.alert('CONFIRMAR', alertMessage, [
                    {
                      text: 'Cancelar',
                      onPress: () => console.log('Cancel Pressed!')
                    }, {
                      text: 'Aceptar',
                      onPress: () => console.log('OK Pressed!')
                    }
                  ])}>
                    <Icon name="print" style={styles.listIcon}/>
                  </Button>
                </Row>
              </Col>
            </Grid>
          </Item>} renderLeftHiddenRow={data => <Button full></Button>} renderRightHiddenRow={(data, secId, rowId, rowMap) => <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)} style={styles.columnItem}>
            <Icon active name="cancel" style={styles.whiteIcon}/>
            <Text style={styles.cancelTxt}>Cancelar</Text>
          </Button>} leftOpenValue={0} rightOpenValue={-95}/>

        </Content>
      </Container>
    )

  }
}

export default CompListView;
