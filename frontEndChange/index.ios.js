import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet
} from 'react-native';
import styles from './components/styles'
import Icon from 'react-native-vector-icons/MaterialIcons'
import SignIn from './components/signIn'
import CompListView from './components/listView'
import CreateView from './components/createView'
import PMScreate from './components/PMScreate'
import IndexMenu from './components/indexMenu'
import GiftsAdd from './components/giftsAdd'
import TabsBar from './components/tabs'
import PreviewForm from './components/previewForm'
import Premanifiesto from './components/premanifiesto'
import { StackNavigator, TabNavigator } from 'react-navigation'

const frontend = StackNavigator({
  Login:{screen:SignIn},
  IndexMenu: { screen: IndexMenu },
  CreateView: { screen: CreateView },
  CompListView: { screen: CompListView },
  PMScreate: { screen: PMScreate },
  GiftsAdd: { screen: GiftsAdd },
  PreviewForm: { screen: PreviewForm },
  TabsBar: { screen: TabsBar },
  Premanifiesto: { screen: Premanifiesto }
})

/*export default class frontend extends Component {
  render() {
    return (
      <CreateView />
    );
  }
}*/

AppRegistry.registerComponent('frontend', () => frontend);
